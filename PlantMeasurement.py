#pip3 install RPI.GPIO
#pip3 install adafruit-circuitpython-ads1x15

#Little of theory about parameters:
#ADS1115 have range -32768 to 32767
#Soil moisture sensor have range 0 to 3V
#ADS1115 in mode gain 1 have range +/- 4.096V
#For 3V is output of ADS cca 23920 level.


import board #To know what board I am using (specific pins, etc...)
import busio #Module contains classes to support serial protocols
import adafruit_ads1x15.ads1115 as ADS #ADC 16bit instance
import RPi.GPIO as GPIO
import datetime
import csv

from adafruit_ads1x15.analog_in import AnalogIn #For reading analog data from ADC
from board import SCL, SDA 
from math import floor
from time import sleep

import mysql.connector

# setup input
i2c = busio.I2C(SCL, SDA)
ads1 = ADS.ADS1115(i2c,address = 0x48) #First ads1115 modul (adress via GND)
ads2 = ADS.ADS1115(i2c,address = 0x49) #Second ads1115 modul (adress via VCC)

#Channels for first modul ads1115
chan10 = AnalogIn(ads1, ADS.P0)
chan11 = AnalogIn(ads1, ADS.P1)
chan12 = AnalogIn(ads1, ADS.P2)
chan13 = AnalogIn(ads1, ADS.P3)

#Channels for second modul ads1115
chan20 = AnalogIn(ads2, ADS.P0)
chan21 = AnalogIn(ads2, ADS.P1)
chan22 = AnalogIn(ads2, ADS.P2)
chan23 = AnalogIn(ads2, ADS.P3)

# pinouts
ser = 8
rclk = 7
srclk = 1

# setup output
GPIO.setmode(GPIO.BCM)
GPIO.setup(ser, GPIO.OUT, initial = 0) #PortOrPin, GPIO.Out, InitialState
GPIO.setup(rclk, GPIO.OUT, initial = 0)
GPIO.setup(srclk, GPIO.OUT, initial = 0)



def logToFile(time, mois_sens_1, mois_sens_2):
    f = open("MoistureData.csv", "a")
    writer = csv.DictWriter(f, fieldnames = ["Time","MoisSen10","MoisSen11","MoisSen12","MoisSen13","MoisSen20","MoisSen21","MoisSen22","MoisSen23"])
    writer.writerow({"Time":time, "MoisSen10":str(mois_sens_1[0]), "MoisSen11":str(mois_sens_1[1]), "MoisSen12":str(mois_sens_1[2]), "MoisSen13":str(mois_sens_1[3]), "MoisSen20":str(mois_sens_2[0]), "MoisSen21":str(mois_sens_2[1]), "MoisSen22":str(mois_sens_2[2]), "MoisSen23":str(mois_sens_2[3])})
    f.close()    

def moist_in_percent(value):
    return floor(100-(value-7000)/130)

def watering_pump_activate(array_for_shift_reg):
    array_for_shift_reg.reverse()
    for x in array_for_shift_reg:
        GPIO.output(ser, x)
        GPIO.output(srclk, 1)
        GPIO.output(srclk, 0)
    GPIO.output(rclk, 1)
    GPIO.output(rclk, 0)
    print("watering")
    sleep(10)
    default_array = [0,0,0,0,0,0,0,0]
    for x in default_array:
        GPIO.output(ser, x)
        GPIO.output(srclk, 1)
        GPIO.output(srclk, 0)
    GPIO.output(rclk, 1)
    GPIO.output(rclk, 0)
    print("end of watering")

def run():
    mois_sens_1 = [0,0,0,0]
    mois_sens_1[0] = moist_in_percent(chan10.value)
    mois_sens_1[1] = moist_in_percent(chan11.value)
    mois_sens_1[2] = moist_in_percent(chan12.value)
    mois_sens_1[3] = moist_in_percent(chan13.value)
    mois_sens_2 = [0,0,0,0]
    mois_sens_2[0] = moist_in_percent(chan20.value)
    mois_sens_2[1] = moist_in_percent(chan21.value)
    mois_sens_2[2] = moist_in_percent(chan22.value)
    mois_sens_2[3] = moist_in_percent(chan23.value)

    print("Sens10:" + str(mois_sens_1[0]) + " Sens11:" + str(mois_sens_1[1]) + " Sens12:" + str(mois_sens_1[2]) + " Sens13:" + str(mois_sens_1[3]) + " [%] ")
    print("Sens20:" + str(mois_sens_2[0]) + " Sens21:" + str(mois_sens_2[1]) + " Sens22:" + str(mois_sens_2[2]) + " Sens23:" + str(mois_sens_2[3]) + " [%] ")
    
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    current_hours = current_time[11:13]
    
    logToFile(current_time, mois_sens_1, mois_sens_2)
    
    mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="passworddb",
    database="plantation"
    )
    mycursor = mydb.cursor()
    sql = "INSERT INTO moisture (time, sensor10, sensor11, sensor12, sensor13, sensor20, sensor21, sensor22, sensor23) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (current_time, str(mois_sens_1[0]), str(mois_sens_1[1]), str(mois_sens_1[2]), str(mois_sens_1[3]), str(mois_sens_2[0]), str(mois_sens_2[1]), str(mois_sens_2[2]), str(mois_sens_2[3]))
    mycursor.execute(sql, val)
    mydb.commit()
    mydb.close()
    
    
    all_mois_sensors = mois_sens_1 + mois_sens_2
    for x in range(len(all_mois_sensors)):
        array_for_shift_reg = [0,0,0,0,0,0,0,0]
        if (all_mois_sensors[x] < 50) and (int(current_hours) < 23 and int(current_hours) > 9):
            array_for_shift_reg[x] = 1
            print("debug: Zalevam: " + str(all_mois_sensors[x]))
            print(array_for_shift_reg)
            watering_pump_activate(array_for_shift_reg)
    return 0

while(True): 
    run()
    sleep(600)

