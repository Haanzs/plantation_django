$(document).ready(function()  {
	console.log(btnclass)
	for (var x = 0; x<4; x++) {
		console.log(btnclass[x])
		if (btnclass[x] == "btn-success") 
			{
				console.log("yep")
				if ($("#btn"+x).className == "btn-success"){}
				else
				{
				$("#btn"+x).removeClass('btn-danger');
				$("#btn"+x).addClass('btn-success');
				}
			}
		else
		{		if ($("#btn"+x).className == "btn-danger"){}
				else
				{
				$("#btn"+x).removeClass('btn-success');
				$("#btn"+x).addClass('btn-danger');
			}
		    }	
		

		   }
console.log("now")
});


var MyChart = new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: labels,
    datasets: [
      { 
        data: values[0],
        label: "Dragon fruit 1/2",
        borderColor: "rgb(255, 0, 0)",
        pointRadius: 1,
        fill: false
      }, 
      { 
        data: values[1],
        label: "Dragon fruit 2/2",
        borderColor: "rgb(255, 128, 0)",
        pointRadius: 1,
        fill: false
      },
      { 
        data: values[2],
        label: "Lemon",
        borderColor: "rgb(255, 255, 0)",
        pointRadius: 1,
        fill: false
      },
      { 
        data: values[3],
        label: "Hawai rose",
        borderColor: "rgb(128, 255, 0)",
        pointRadius: 1,
        fill: false
      },
      { 
        data: values[4],
        label: "Fairy light",
        borderColor: "rgb(0, 255, 255)",
        pointRadius: 1,
        fill: false
      },
      { 
        data: values[5],
        label: "Purple flash",
        borderColor: "rgb(0, 0, 255)",
        pointRadius: 1,
        fill: false
      },
      { 
        data: values[6],
        label: "ThanksGiving",
        borderColor: "rgb(255, 0, 255)",
        pointRadius: 1,
        fill: false
      },
      { 
        data: values[7],
        label: "Sparkler",
        borderColor: "rgb(255, 0, 127)",
        pointRadius: 1,
        fill: false
      },
    ]
  },
  options: {
      title: {
	display: true,
	text: 'None'
      },
      scales: {
	      y: {
		  max: 100,
		  min: 20,
		  ticks: {
		      stepSize: 10
		  }
	      }
	  },
      elements: {
	line: {
	    tension: 0, // disables bezier curves
	    borderWidth: 3
	}
    },
  }
});
MyChart.draw();
