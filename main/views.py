from django.shortcuts import render
from django.http import HttpResponse
from django.conf.urls.static import static

import pandas as pd
import mysql.connector

from django import template
register = template.Library()

@register.filter
def get_by_index(l, i, i2 = None):
	if i2 != None:
		return l[i][i2]
	else:
		return l[i]

def find_last_watering_date(dataset):
    for i in range(1, len(dataset)+1, 1):
        if (dataset[-i]-dataset[-i-1] > 2) and (dataset[-i]-dataset[-i-2] > 2) and (dataset[-i]-dataset[-i-3] > 2):
            return i+1
            
#Prepare data from Datbase
def prepare_data_from_database():
    mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="passworddb",
    database="plantation"
    )

    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM moisture")
    myresult = mycursor.fetchall()
    all_data = pd.DataFrame(myresult, columns =['time', 'sensor10', 'sensor11', 'sensor12', 'sensor13', 'sensor20', 'sensor21', 'sensor22', 'sensor23'])
    all_data["Hour"] = str(all_data["time"])[11:13]
    all_data["HourAndMin"] = str(all_data["time"])[11:15]

    Sensor10 = all_data.sensor10
    Sensor11 = all_data.sensor11
    Sensor12 = all_data.sensor12
    Sensor13 = all_data.sensor13
    Sensor20 = all_data.sensor20
    Sensor21 = all_data.sensor21
    Sensor22 = all_data.sensor22
    Sensor23 = all_data.sensor23
    
    
    Sensor0 = all_data.sensor10
    Sensor1 = all_data.sensor11
    Sensor2 = all_data.sensor12
    Sensor3 = all_data.sensor13
    Sensor4 = all_data.sensor20
    Sensor5 = all_data.sensor21
    Sensor6 = all_data.sensor22
    Sensor7 = all_data.sensor23
        

    x_values = []
    for t1 in all_data["time"]:
        x1 = str(t1)
        x_values.append(x1)

    y_values = []
    y_values.append(all_data["sensor10"].to_list()) 
    y_values.append(all_data["sensor11"].to_list())
    y_values.append(all_data["sensor12"].to_list())
    y_values.append(all_data["sensor13"].to_list())
    y_values.append(all_data["sensor20"].to_list())
    y_values.append(all_data["sensor21"].to_list())
    y_values.append(all_data["sensor22"].to_list())
    y_values.append(all_data["sensor23"].to_list())  
    
    
    last_watered_time = []
    for sets in range(len(y_values)):
        last = find_last_watering_date(y_values[sets])
        last_watered_time.append(x_values[-last])
    
    
    mydb.close()
    return x_values, y_values, last_watered_time



# Create your views here.

def index(response):  
	labels,values,last_watered_time = prepare_data_from_database()
	#print(labels)
	
	return render(response, "main/base.html", {"labels":labels, "values":values, "LastWateredTime":last_watered_time, "noha":1})
	
def home(response):
	labels,values,last_watered_time = prepare_data_from_database()
	print("DEFAULT -  Last 24 hours")
	print(labels[-1])
			
	if response.method == "POST":	
		if response.POST.get("Since the beginning of time."):
			print("Preseed Since the beginning of time.")
			btnclass = ["btn-danger", "btn-success", "btn-success", "btn-success"]
		if response.POST.get("Last 24 hours"):
			print("Pressed Last 24 hours")
			labels = labels[-145:]
			for x in range(len(values)):
				values[x] = values[x][-145:]
			btnclass = ["btn-success", "btn-success", "btn-success", "btn-danger"]
		if response.POST.get("Last 48 hours"):
			print("Pressed Last 48 hours")
			labels = labels[-290:]
			for x in range(len(values)):
				values[x] = values[x][-290:]
			btnclass = ["btn-success", "btn-success", "btn-danger", "btn-success"]
		if response.POST.get("Last 1 week"):
			print("Pressed Last 1 week")
			labels = labels[-1015:]
			for x in range(len(values)):
					values[x] = values[x][-1015:]
			btnclass = ["btn-success", "btn-danger", "btn-success", "btn-success"]
	else:
		labels = labels[-145:]
		for x in range(len(values)):
			values[x] = values[x][-145:]
			btnclass = ["btn-success", "btn-success", "btn-success", "btn-danger"]

	print(labels[-1])
	return render(response, "main/home.html", {"labels":labels, "values":values, "LastWateredTime":last_watered_time, "btnclass":btnclass})
	
	
	
